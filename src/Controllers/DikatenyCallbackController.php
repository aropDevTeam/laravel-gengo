<?php

namespace Tokido\Dikateny\Controllers;

use App;
use Gengo;
use Illuminate\Http\Request;
use Tokido\Dikateny\Dikateny;
use Illuminate\Support\Facades\Log;
use Illuminate\Routing\Controller as BaseController;
use Tokido\Dikateny\Model\TranslationRequestJob;
use Tokido\Dikateny\Model\TranslationRequestJobResponse;
use Tokido\Dikateny\Model\TranslationRequestOrder;
use Tokido\Dikateny\Model\TranslationRequestJobComment;
use Storage;

class DikatenyCallbackController extends BaseController
{

    public function __construct(
        Dikateny $translator
    ){
        $this->translator = $translator;
    }

    public function callback(Request $request)
    {
        $response = $request->all();

        \Log::info('CALLBACK RECEIVED', $response);

        if(isset($response) && is_array($response) && array_key_exists('job', $response)) {
            $this->saveCallbackRequestAsResponse($response);
        } else {
            \Log::info('CALLBACK NO JOB', $response);
        }

        if(isset($response) && is_array($response) && array_key_exists('comment', $response)) {
            $this->saveCallbackRequestAsComment($response);
        }
    }

    public function saveCallbackRequestAsResponse($response)
    {
        $responseData = json_decode($response['job'], true);
        if (!isset($responseData['slug'])) {
            $responseData['slug'] = $responseData['custom_data']??'';
        }
        if(is_array($responseData) && array_key_exists('status', $responseData)) {

            if(array_key_exists('status', $responseData) && ($responseData['status'] == 'available' || $responseData['status'] == 'pending')) {
                $job = TranslationRequestJob::where('job_id', $responseData['job_id'])->first();
                if (!$job instanceof TranslationRequestJob) {
                    \Log::info('CALLBACK CREATE JOB', $responseData);
                    $job = $this->saveAvailableJobsForOrder($responseData, $responseData['order_id']);
                } else {
                    \Log::info('CALLBACK NO CREATE JOB', $responseData);
                }
            } else {
                \Log::info('CALLBACK NO AVAILABLE / PENDING', $responseData);
            }

            if(array_key_exists('status', $responseData) && ($responseData['status'] == 'reviewable')) {
                $job = $this->updateJobStatus($responseData, $responseData['status']);
                
                if ($job) {
                    $order = TranslationRequestOrder::find($job->translation_request_order_id);

                    $jobResponse = new TranslationRequestJobResponse($responseData);
                    $job->callbackResponses()->save($jobResponse);

                    $order->save();
                }
            } elseif (array_key_exists('status', $responseData)) {
                $job = $this->updateJobStatus($responseData, $responseData['status']);

                if ($responseData['status'] == 'approved') {
                    $job = TranslationRequestJob::where('job_id', $responseData['job_id'])->first();
                    if ($job) {
                        $order = TranslationRequestOrder::find($job->translation_request_order_id);
                        $jobResponse = new TranslationRequestJobResponse($responseData);
                        $job->callbackResponses()->save($jobResponse);
                        $this->applyApprovedJob($order);
                    }
                    
                }
            }
            
            $this->updateOrderRequestStatus($responseData['order_id']);
        } else {
            \Log::info('CALLBACK JOB ERROR 01', $responseData);
        }
    }

    public function applyApprovedJob($order)
    {
        $objectclass = $order->translatable_type;
        $object = $objectclass::find($order->translatable_id);
        $translations = $object->getApprovedTranslatableValues($order->id);

        // \Log::info($translations);
        $status = $object->applyTranslations($translations);
    }

    public function updateOrderRequestStatus($idOrder)
    {
        if (isset($idOrder)) {
            $order = TranslationRequestOrder::where('order_id', $idOrder)->first();
            if ($order instanceof TranslationRequestOrder) {
                if (count($order->jobs) > 0) {
                    $jobCount = count($order->jobs);
                    $jobAvailable = 0;
                    $jobPending = 0;
                    $jobReviewable = 0;
                    $jobCancelled = 0;
                    $jobApproved = 0;

                    foreach ($order->jobs as $job) {
                        if ($job->status == 'available') {
                            $jobAvailable++;
                        } elseif ($job->status == 'pending') {
                            $jobPending++;
                        } elseif ($job->status == 'reviewable') {
                            $jobReviewable++;
                        } elseif ($job->status == 'cancelled') {
                            $jobCancelled++;
                        } elseif ($job->status == 'approved') {
                            $jobApproved++;
                        }
                    }

                    if ($jobCount == $jobAvailable) {
                        $order->status = 'new';
                    } else {
                        if (
                            $jobCount == $jobCancelled
                            || $jobCount == $jobApproved
                            || $jobCount == ($jobApproved + $jobCancelled)
                        ) {
                            $order->status = 'closed';
                        } elseif ($jobCount == $jobReviewable) {
                            $order->status = 'reviewable';
                        } else {
                            $order->status = 'processing';
                        }
                    }

                    // If auto validation
                    // if ($jobCount == $jobReviewable) {
                    //     if(!$order->wait_validation) {
                    //         $this->completeAndApplyTranslation($order);
                    //     }
                    // }
                    
                    // If manual validtaion
                    if ($jobCount == $jobApproved) {
                        $this->completeAndApplyTranslation($order);
                    }

                    $order->save();
                }
            }
        }
    }

    public function updateJobStatus($responseData, $status)
    {
        $job = TranslationRequestJob::where('job_id', $responseData['job_id'])->first();
        if($job) {
            $job->status = $status;
            $job->save();
        }

        return $job;
    }

    public function saveAvailableJobsForOrder($responseData, $order_id)
    {
        $order = TranslationRequestOrder::where('order_id', $order_id)->first();
        if($order) {

            $existingJob = TranslationRequestJob::where('job_id', $responseData['job_id'])->first();
            if(!$existingJob) {
                $requestJob = new TranslationRequestJob($responseData);
                $order->jobs()->save($requestJob);
                \Log::info('CALLBACK JOB CREATED', $responseData);
            } else {
                \Log::info('CALLBACK JOB ALREADY EXISTS', $responseData);
            }

            // $jobs = $this->requestJobFromOrder($order);
            // if($jobs && $jobs != null && $jobs['opstat'] && $jobs['opstat'] == 'ok' && $jobs['response'] && $jobs['response']['jobs']) {
            //     foreach ($jobs['response']['jobs'] as $job) {
            //         $existingJob = TranslationRequestJob::where('job_id', $job['job_id'])->first();
            //         if(!$existingJob) {
            //             $requestJob = new TranslationRequestJob($job);
            //             $order->jobs()->save($requestJob);
            //             \Log::info('CALLBACK JOB CREATED', $job);
            //         } else {
            //             \Log::info('CALLBACK JOB ALREADY EXISTS', $job);
            //         }
            //     }    
            // } else {
            //     \Log::info('CALLBACK JOBS FROM ORDER ISSUES', ["jobs" => $jobs]);
            // }
            // return $jobs;
        } else {
            \Log::info('CALLBACK ORDER NOT FOUND', $order_id);
            return false;
        }
    }

    public function requestJobFromOrder($order)
    {
        $orderData = $this->translator->getOrder($order->order_id);
        \Log::info('CALLBACK ORDER FROM API BEFORE SAVE', $orderData);
        $orderIds  = $this->translator->getAllJobIdsAndStatusFromOrder($orderData, ['jobs_available', 'jobs_pending', 'jobs_reviewable', 'jobs_approved', 'jobs_revising', 'jobs_cancelled', 'jobs_rejected', 'jobs_held']);

        if($orderIds && sizeof($orderIds) > 0) {
            $response = $this->translator->getJobsByIds($orderIds);
            \Log::info('CALLBACK JOB IDS', $orderIds);
        } else {
            \Log::info('CALLBACK JOB IDS EMPTY', []);
            $response = null;
            throw new \Exception('$orderIds is EMPTY');
        }
        return $response;   
    }

    public function saveCallbackRequestAsComment($response)
    {
        $responseData = json_decode($response['comment'], true);
        if(is_array($responseData) && array_key_exists('body', $responseData) && array_key_exists('job_id', $responseData)) {
            $job = TranslationRequestJob::where('job_id', $responseData['job_id'])->first();
            $comment = new TranslationRequestJobComment($responseData);
            if ($job) {
                $job->translationsComments()->save($comment);
            }
        }
    }

    public static function getCallbackStatus()
    {
        return array(
            'queued',
            'available',
            'pending',
            'reviewable',
            'approved',
            'revising',
            'rejected',
            'canceled',
            'hold'
        );
    }

    public function completeAndApplyTranslation($order) 
    {
        $order->jobs()->update(['status' => 'approved']);
        $order->status = 'closed';
        $order->save();

        foreach ($order->jobs as $approvableJob) {
            $this->updateJobAndResponseStatus($approvableJob);
        }

        $objectclass = $order->translatable_type;
        $object = $objectclass::find($order->translatable_id);
        $translations = $object->getApprovedTranslatableValues($order->id);

        $status = $object->applyTranslations($translations);
        
        return true;
    }

    public function updateJobAndResponseStatus ($approvableJob)
    {
        $responseJob = $approvableJob->callbackResponses()->orderBy('created_at', 'desc')->first();
        if($responseJob) {
            $responseJob->status = 'approved';
            if (!$responseJob->save()) 
                \Log::info("Failed to save!");
        }
    }

    public function isCompleted($order)
    {
        foreach ($order->jobs as $job) {
            if($job->status != 'reviewable' && $job->status != 'approved')
                return false;
        }

        return true;
    }
}