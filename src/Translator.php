<?php

namespace Tokido\Dikateny;

use App;
use Illuminate\Http\Request;
use Tokido\Dikateny\Dikateny;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Tokido\Dikateny\Model\TranslationRequestJob;
use Tokido\Dikateny\Model\TranslationRequestOrder;
use Tokido\Dikateny\Model\TranslationRequestJobComment;
use Tokido\Dikateny\Model\TranslationRequestJobRevision;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder as QueryBuilder;

trait Translator
{
    protected $defaultLocale;

    protected static $dikateny; 

    public static function bootTranslator()
    {
        static::$dikateny = \App::make(Dikateny::class, array( 
                config('dikateny.api.key'),
                config('dikateny.api.privatekey')
            )
        );
    }


    public static function getTranslatables()
    {
        /*need reflection to work */
        $instance = new self();
        return $instance->autoTranslatables;
    }

    public function serializeTranslatablesValues()
    {

        $serialized = array();
        foreach ($this->autoTranslatables as $translatableValues) {
            $serialized[$translatableValues] = $this->$translatableValues;
        }
        return $serialized;
    }

    public function submitTranslationOrder( $tier = "standard", $force = 1, $lc_src = "fr", $lc_tgt = "en", $fields = [], $comment = "", $type = "text", $auto_approve = false)
    {
        $jobList = array();

        foreach ($fields as $field => $isChecked) {
            if ($isChecked === '1') {
                
                $slug = "";
                $explode = explode(':', $field);
                $body_src = "";
                if ($explode[0] === 'seo') { // SEO field
                    $slug = strtolower((new \ReflectionClass($this))->getShortName() . '_' .$this->id . '_' . $field);
                    $seo = $this->seo;
                    if ($seo) {
                        $body_src = $this->seo->{$explode[1].':'.$lc_src};
                    }
                } else {
                    if (isset($this->type_event)) {
                        $slug = strtolower((new \ReflectionClass($this))->getShortName() . '_' .$this->single_evenement->id . '_' . $field);
                        if ($field == 'title' || $field == 'presentation'){
                            $body_src = $this->single_evenement->{$field.':'.$lc_src};
                        } else {
                            $body_src = $this->{$field.':'.$lc_src};
                        }
                    } else {
                        $slug = strtolower((new \ReflectionClass($this))->getShortName() . '_' .$this->id . '_' . $field);
                        $body_src = $this->{$field.':'.$lc_src};
                    }
                }

                if (!empty($body_src) && $body_src != '<p><br></p>') {
                    $job = self::$dikateny->createJob(
                        $slug,
                        $body_src,
                        $type,
                        $tier,
                        $force,
                        $lc_src,
                        $lc_tgt,
                        $comment,
                        $auto_approve
                    );
                    $job['custom_data'] = $job['slug'];
                    array_push($jobList, $job);
                }
            }
            
        }
        \Log::info('SEND REQUEST FOR GENGO HERE', $jobList);
        $order = self::$dikateny->postJobs($jobList);
        return $order;   
    }

    // public function encodeHTMLCharacter($body_src)
    // {
    //     $result = str_replace('&amp;', '[[[___amp;]]]', $body_src);
    //     $result = str_replace('&nbsp;', '[[[___nbsp;]]]', $result);
    //     $result = str_replace('&', '[[[###amp;]]]', $result);
    //     return $result;
    // } 

    // public function decodeHTMLCharacter($body_src)
    // {
    //     $result = str_replace('[[[###amp;]]]', '&', $body_src);
    //     $result = str_replace('[[[___amp;]]]', '&amp;', $result);
    //     $result = str_replace('[[[___nbsp;]]]', '&nbsp;', $result);
    //     return $result;
    // } 

    public function saveOrder($order, $needValidation = false, $translationForce = 1, $lc_src = "fr", $lc_tgt = "en", $tier = "pro")
    {
        $requestOrder = null;
        if($order && $order['opstat'] && $order['opstat'] == 'ok') {
            $requestOrder =  new TranslationRequestOrder($order['response']);
            $requestOrder->wait_validation = $needValidation;
            $requestOrder->translation_force = $translationForce;
            $requestOrder->lc_src = $lc_src;
            $requestOrder->lc_tgt = $lc_tgt;
            $requestOrder->tier = $tier;
            $requestOrder->save();
        }
        
        return $requestOrder;
    }

    /*public function requestJobFromOrder($order)
    {
        $orderData = self::$dikateny->getOrder($order->order_id);
        $orderIds = self::$dikateny->getAllJobIdsAndStatusFromOrder($orderData, ['jobs_available']);
        if($orderIds && sizeof($orderIds) > 0) {
            $response = self::$dikateny->getJobsByIds($orderIds);
        } else {
            $response = null;
        }
        return $response;   
    }*/

    public function requestTranslations($needValidation = false, $tier = "pro", $force = 1, $lc_src = "fr", $lc_tgt = "en", $fields = [], $comment = '')
    {
        $order = $this->submitTranslationOrder($tier, $force, $lc_src, $lc_tgt, $fields, $comment, $type = "text", $needValidation ? false : true);

        // dd($fields);
        if (is_array($order)) {
            if (isset($order['opstat']) && $order['opstat'] == 'error') {
                return [
                    'error' => 'L\'API Gengo a retourné une erreur: code ' . $order['err']['code'] . ' - ' . $order['err']['msg']
                ];
            }
        }
        $requestOrder = $this->saveOrder($order, $needValidation, $force, $lc_src, $lc_tgt, $tier);
        $result = $this->translationRequestOrders()->save($requestOrder);

        if ($result instanceof TranslationRequestOrder) {
            return true;
        } else {
            return [
                'error' => 'Il y a eu un souci lors de l\'enregistrement de votre demande'
            ];
        }

        /*useless; get from the callback*/
        
        /*$jobs = $this->requestJobFromOrder($requestOrder);
        
        if($jobs && $jobs != null && $jobs['opstat'] && $jobs['opstat'] == 'ok' && $jobs['response'] && $jobs['response']['jobs']) {
            foreach ($jobs['response']['jobs'] as $job) {
                $requestJob = new TranslationRequestJob($job);
                $requestOrder->jobs()->save($requestJob);
            }    
        }*/
    }

    public static function requestTranslationById($id, $request, $comite = false)
    {
        // dd($request);
        $validateResult = self::validateTranslationRequest($id, $request, $comite);
        if ($validateResult === true) {
            $tier = $request['tier'];
            $force = 1;

            $object = self::find($id);
            return $object->requestTranslations(isset($request['translation_validation']) ? true : false, $tier, $force, $request['translation_lg_src'], $request['translation_lg_tgt'], $request['translates'], $request['translation_comment']);
        } else {
            return $validateResult;
        }
        
    }

    public static function validateTranslationRequest($id, $request, $comite = false)
    {
        $object = self::find($id);
        // dd($object);
        $errors = [];
        if (isset($request['translates']) && is_array($request['translates']) && count($request['translates']) > 0) {
            // Check if source or target languange is empty
            if (!isset($request['translation_lg_src'])  || !isset($request['translation_lg_tgt'])) {
                $errors['langue_empty'] = 'La langue source et la langue cible ne peuvent pas être vides.';
            }
            
            // Check if source != target
            if ($request['translation_lg_src'] == $request['translation_lg_tgt']) {
                $errors['same_langue'] = 'La langue source et la langue cible ne peuvent pas être identiques.';
            } else {
                // Check if field is empty
                $allEmpty = true;
                foreach ($request['translates'] as $field => $isChecked) {
                    $explode = explode(':', $field);
                    if ($explode[0] === 'seo') { // SEO field
                        $seo = $object->seo;
                        if ($seo) {
                            if (!empty($seo->{$explode[1].':'.$request['translation_lg_src']})) {
                                $allEmpty = false;
                            } else {
                                $errors[$field] = 'La demande de traduction ' . strtoupper($request['translation_lg_src']) . ' vers ' . strtoupper($request['translation_lg_tgt']) . ' ne peut pas aboutir car aucune valeur n\'est enregistrée pour le champs "'. self::getTranslatables()[$field] .  ' '.strtoupper($request['translation_lg_src']) . '"';
                            }
                        }
                    } else {
                        // dd(empty($object->{$field.':'.$request['translation_lg_src']}));
                        if (isset($object->type_event)) { // If evenement
                            if ($field == 'title' || $field == 'presentation') {
                                // \Log::info($object->single_evenement);
                                // \Log::info($object->single_evenement->{$field.':'.$request['translation_lg_src']});
                                // \Log::info($object->single_evenement->{$field.':'.$request['translation_lg_src']});
                                if (!empty($object->single_evenement->{$field.':'.$request['translation_lg_src']})) {
                                    $allEmpty = false;
                                } else {
                                    $errors[$field] = 'La demande de traduction ' . strtoupper($request['translation_lg_src']) . ' vers ' . strtoupper($request['translation_lg_tgt']) . ' ne peut pas aboutir car aucune valeur n\'est enregistrée pour le champs "'. self::getTranslatables()[$field] .  ' '.strtoupper($request['translation_lg_src']) . '"';
                                }
                            } else {
                                if (!empty($object->{$field.':'.$request['translation_lg_src']})) {
                                    $allEmpty = false;
                                } else {
                                    $errors[$field] = 'La demande de traduction ' . strtoupper($request['translation_lg_src']) . ' vers ' . strtoupper($request['translation_lg_tgt']) . ' ne peut pas aboutir car aucune valeur n\'est enregistrée pour le champs "'. self::getTranslatables()[$field] .  ' '.strtoupper($request['translation_lg_src']) . '"';
                                }
                            }
                            
                        } else {
                            if (empty($object->{$field.':'.$request['translation_lg_src']}) || $object->{$field.':'.$request['translation_lg_src']} == '<p><br></p>') {
                                // \Log::info(self::getTranslatables());
                                $fieldname = self::getTranslatables()[$field];
                                if ($comite) {
                                    if ($field == 'texte_accroche_home') {
                                        $fieldname = 'titre';
                                    }
                                    if ($field == 'texte_presentation_en_savoir_plus') {
                                        $fieldname = 'description';
                                    }
                                }
                                $errors[$field] = 'La demande de traduction ' . strtoupper($request['translation_lg_src']) . ' vers ' . strtoupper($request['translation_lg_tgt']) . ' ne peut pas aboutir car aucune valeur n\'est enregistrée pour le champs "'. $fieldname .  ' '.strtoupper($request['translation_lg_src']) . '"';
                            } else {
                                $allEmpty = false;
                            }
                        }
                        
                    }
                }

                // Check if a same demande already exist and is not approuved yet
                if (count($object->translationRequestOrders) > 0) {
                    foreach ($object->translationRequestOrders as $order) {
                        if ($order->status != 'closed' && $order->status != 'canceled' && $order->lc_src == $request['translation_lg_src'] && $order->lc_tgt == $request['translation_lg_tgt']) {
                            $errors['langue_processing'] = 'Une demande ' . strtoupper($request['translation_lg_src']) . ' vers ' . strtoupper($request['translation_lg_tgt']) . ' est déjà en cours.';
                        }

                        // Also Check if the source or target langue is the same as the target of a 'non closed' order
                        if ($order->status != 'closed' && $order->status != 'canceled' && ($order->lc_tgt == $request['translation_lg_src'] || $order->lc_tgt == $request['translation_lg_tgt'])) {
                            $errors['langue_processing'] = 'Vous ne pouvez pas demander une traduction ' . strtoupper($request['translation_lg_src']) . ' vers ' . strtoupper($request['translation_lg_tgt']) . ' parce qu\'une demande ' . strtoupper($order->lc_src) . ' vers '. strtoupper($order->lc_tgt) . ' est déjà en cours.';
                        }

                        // Also Check if the target langue is the same as the source of a 'non closed' order
                        if ($order->status != 'closed' && $order->status != 'canceled' && ($order->lc_src == $request['translation_lg_tgt'])) {
                            $errors['langue_processing_2'] = 'Vous ne pouvez pas demander une traduction ' . strtoupper($request['translation_lg_src']) . ' vers ' . strtoupper($request['translation_lg_tgt']) . ' parce qu\'une demande ' . strtoupper($order->lc_src) . ' vers '. strtoupper($order->lc_tgt) . ' est déjà en cours.';
                        }
                    }
                }

            }

            

        } else {
            $errors = [
                'translates' => 'Au moins un des champs traductibles doit être coché.'
            ];
        }

        if (empty($errors)) {
            return true;
        } elseif (isset($allEmpty) && !$allEmpty) {
            if (
                isset($errors['langue_processing'])
                || isset($errors['langue_empty'])
                || isset($errors['same_langue'])
                || isset($errors['langue_processing_2'])
            ) {
                return $errors;
            } else {
                return true;
            }
        } else {
            return $errors;
        }
        // dd($request, $object->translationRequestOrders);
    }

    public function checkRequestedTranslationStatus()
    {
           
    }

    public function cancelLastRequestOrderById($orderId)
    {
        return self::$dikateny->cancelOrderByID($orderId);
    }

    public static function cancelRequestJobById($jobId)
    {
        $response =  self::$dikateny->cancelJobByID($jobId);
        if(is_array($response) && array_key_exists('opstat', $response) && $response['opstat'] == 'ok') {
            $job = TranslationRequestJob::where('job_id', $jobId)->first();
            if($job) {
                $job->status = "cancelled";
                $job->save();
            }
            return $job;
        }

        return null;
    }

    public static function cancelRequestJobByIds(array $jobIds)
    {
        $responses = array();

        foreach ($jobIds as $jobId) {
            $response = self::cancelRequestJobById($jobId);
            if ($response != null)
                array_push($responses, $response);
        }
        return $responses;
    }

    public function cancelAllCurrentRequestOrder()
    {
        $cancelledJobsForOrder = array();
        
        /*For last Request Only :*/
        $requestOrder = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        
        //foreach ($this->translationRequestOrders as $requestOrder) {
        $response = $this->cancelLastRequestOrderById($requestOrder->order_id);
        if(is_array($response) && array_key_exists('opstat', $response) && $response['opstat'] == 'ok') {
            if(is_array($response['response']) && array_key_exists('order', $response['response']) && array_key_exists('jobs_cancelled', $response['response']['order']) && sizeof($response['response']['order']['jobs_cancelled'])>0) {
                foreach ($response['response']['order']['jobs_cancelled'] as $cancelled) {
                    $job = TranslationRequestJob::where('job_id', $cancelled)->first();
                    if($job) {
                        $job->status = "cancelled";
                        $job->save();
                        array_push($cancelledJobsForOrder, $job);
                    }
                }
            }
        }
        //}
        return $cancelledJobsForOrder;
    }

    public static function cancelAllCurrentRequestOrderByObjectId($id)
    {   
        $object = self::find($id);
        $cancelledJobs = $object->cancelAllCurrentRequestOrder();

        return $cancelledJobs;
    }

    public function postCommentOnLastOrder($body)
    {
        $order = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        return self::postOrderCommentByOrderId($order->order_id, $body);
    }

    public static function postCommentOnLastOrderById($id, $body)
    {
        $object = self::find($id);
        if($object) {
            return $object->postCommentOnLastOrder($body);
        }
        return null;
    }

    public static function postOrderCommentByOrderId($orderId, $body)
    {
        $response = self::$dikateny->commentOrder($orderId, $body);
        if(is_array($response) && $response['opstat'] == 'ok') {
            $comment = new TranslationRequestJobComment([
                'body'      => $body,
                'author'    => 'admin',
                'order_id'  => $orderId
            ]);

            $order = TranslationRequestOrder::where('order_id', $orderId)->first();
            $order->translationsComments()->save($comment);

            return $comment;
        } 
        return null;
    }

    public static function postJobCommentByJobId($jobId, $body)
    {
        $response = self::$dikateny->commentJob($jobId, $body);
        if(is_array($response) && $response['opstat'] == 'ok') {
            $comment = new TranslationRequestJobComment([
                'body'      => $body,
                'author'    => 'admin',
                'job_id'  => $jobId
            ]);

            $job = TranslationRequestJob::where('job_id', $jobId)->first();
            $job->translationsComments()->save($comment);

            return $comment;
        } 
        return null;
    }

    /*single revision for multiple Jobs*/
    public static function reviseSingleJobById($jobId, $body)
    {
        $response = self::$dikateny->reviseJob($jobId, $body);
        if(is_array($response) && $response['opstat'] == 'ok') {
            $revisionData = self::saveRevisionsComment($jobId, $body);
            $job = TranslationRequestJob::where('job_id', $jobId)->first();
            if($job) {
                $job->status = 'revising';
                $job->save();

                $job->translationRevisions()->save($revisionData);
            }

            return $revisionData;
        }
        return null;
    }

    /*Single revision for multiple jobs*/
    public static function reviseMultipleJobByIds(array $jobIds, $body)
    {
        $responses = array();
        $response = self::$dikateny->reviseJobs($jobIds, $body);
        /*check for each response in array*/
        if($response && is_array($response)) {
            foreach ($jobIds as $job) {
                $revisionData = self::saveRevisionsComment($job['job_id'], $body);
                $job = TranslationRequestJob::where('job_id', $job['job_id'])->first();
                if($job) {
                    $job->translationRevisions()->save($revisionData);
                    array_push($responses, $revisionData);
                }
            }
        }
        return $responses;
    }

    /*multiple revisions with multiple comments*/
    public static function reviseJobsWithComments(array $jobs)
    {
        $responses = array();
        foreach ($jobs as $jobId => $comment) {
            $response = self::$dikateny->reviseJob($jobId, $comment);
            array_push($responses, $response);
        }
        return $responses;
    }

    public static function saveRevisionsComment($job_id, $comment)
    {
        $revision = new TranslationRequestJobRevision(
            [
                'job_id'    => $job_id,
                'comment'   => $comment
            ]
        );
        return $revision;
    }

    public static function approveSingleJob($job_id, $rating = 3 , $for_translator = null, $for_mygengo = null, $public = false)
    {
        /*save as approved*/
        $job = TranslationRequestJob::where('job_id', $job_id)->first();
        $response = self::$dikateny->approveJob($job_id, $rating , $for_translator, $for_mygengo, $public);
        if($response && $response['opstat'] == 'ok') {
            $job->status = 'approved';
            $job->save();
        }
        return $response;
    }

    /*approve multiple jobs with same params*/
    public static function approveMultipleJobsWithSameNote($jobIds, $rating = 3 , $for_translator = null, $for_mygengo = null, $public = false)
    {
        $approvals = array();
        foreach ($jobIds as $jobId) {
            $approval = self::$dikateny->buildJobApprovalArgs($jobId, $rating, $for_translator, $for_mygengo, $public);
            array_push($approvals, $approval);
        }

        return self::$dikateny->approveMultipleJobs($approvals);
    } 

    /*approve multiple jobs with different params*/
    public static function approveMultipleJobsWithdiffNote($jobsApprovals)
    {
        $approvals = array();
        foreach ($jobsApprovals as $jobsApproval) {
            extract($jobsApproval);
            array_push($approvals, self::$dikateny->buildJobApprovalArgs($jobId, $rating, $for_translator, $for_mygengo, $public));
        }

        return self::$dikateny->approveMultipleJobs($approvals);
    }

    /*Reject Single Job*/
    public static function rejectSingleJob($job_id,$comment, $captcha, $follow_up = "requeue", $reason = "quality")
    {

        $job = TranslationRequestJob::where('job_id', $job_id)->first();
        $response =  self::$dikateny->rejectSingleJob($job_id,$comment, $captcha, $follow_up, $reason);
        if($response && $response['opstat'] == 'ok') {
            $job->status = 'rejected';
            $job->save();
        }
        return $response;
    } 

    /*Public function reject multiple Jobs*/
    public static function rejectMultipleJobs($jobsRejectRequests, $comment)
    {
        $requests = array();

        foreach ($jobsRejectRequests as $jobsRejectRequest) {
            extract($jobsRejectRequest);
            array_push($requests, self::$dikateny->builtRejectJobRequests($jobId, $comment, $captcha));
        }

        return self::$dikateny->rejectMultipleJob($requests, $comment);
    }      

    /*archive single Job*/
    public static function archiveSingleJob($job)
    {
        return self::$dikateny->archiveSingleJob($job);
    }

    public static function archiveMultipleJobs($jobIds)
    {
        return self::$dikateny->archiveMultipleJobs($jobIds);
    } 

    public static function getTranslationTierList()
    {
        return array(
            'standard'  => 'Standard',
            'pro'       => 'Pro'
        );
    }

    public static function getTranslationForce()
    {
        $force = array();

        for ($i=1; $i <= 10; $i++) { 
            $force[$i] = $i;
        }

        return $force;
    }

    public function checkCurrentStatusTranslation()
    {
        $currentTranslationRequest = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        if($currentTranslationRequest) {
            if($currentTranslationRequest->jobs->count() > 1) {
                $initial = null;
                foreach ($currentTranslationRequest->jobs as $job) {
                    if($job->status == 'pending')
                        return 'pending';   

                    if($job->status == 'revising')
                        return 'revising';                 
                }
                return $currentTranslationRequest->status;
            } else {
                return $currentTranslationRequest->status;
            }
        }
        return 'aucune';
    }

    public function checkCurrentApprovabilityStatus()
    {
        $currentTranslationRequest = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        if($currentTranslationRequest) {
            return $currentTranslationRequest->wait_validation;
        }
        return false;
    }

    public function checkValidTranslationsLanguages()
    {
        return array(

        );
    }

    public function setLanguageCompatibility()
    {
        return array(

        );
    }

    public function getLanguageSrc()
    {
        $order = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        if($order) {
            if($order->jobs()->first())
                return $order->jobs()->first()->lc_src;
        }
        return null;
    }

    public function getLanguageTgt()
    {
        $order = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        if($order) {
            if($order->jobs()->first())
                return $order->jobs()->first()->lc_tgt;
        }
        return null;
    }

    public function getIsValidationNeeded()
    {
        $order = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        if($order) {
            return $order->wait_validation;
        }
        return false;
    }

    public function getTranslationForceTranslation()
    {
        $order = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        if($order) {
            return $order->translation_force;
        }
        return 1;
    }

    public function getTranslationQuality()
    {
        $order = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        if($order) {
            if($order->jobs()->first())
                return $order->jobs()->first()->tier;
        }
        return null;  
    }

    public function GetAppliableReviewableTranslations()
    {
        return $this->getLastAppliableTranslations();
    }

    public static function GetAppliableReviewableTranslationsById($id)
    {
        $object = self::find($id);
        if($object) {
            return $object->GetAppliableReviewableTranslations();
        }

        return array();
    }

    public function getActiveOrderById()
    {
        return $this->translationRequestOrders()->orderBy('created_at', 'desc')->first()->id;
    }

    public function getLastAppliableTranslations()
    {
        $translationRequest = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        $translations = array();

        if($translationRequest) {
            foreach ($translationRequest->jobs as $job) {
                $response = $job->callbackResponses()->orderBy('created_at', 'desc')->first();
                if($response) {
                    array_push($translations, array('translatable' => $job->slug, 'value' => $response));
                }
            }
        }

        return $translations;
    }

    public static function getTranslationStatusList()
    {
        return array(
            'aucune'        => 'Aucune demande',
            'queued'        => 'En cours d\'envoie',
            'available'     => 'En attente de traduction',
            'pending'       => 'En cours de traduction',
            'reviewable'    => 'A valider par AROP',
            'approved'      => 'Approuvé',
            'revising'      => 'En cours de révision',
            'rejected'      => 'Rejetée par AROP',
            'canceled'      => 'Annulée par AROP',
            'hold'          => 'Mise en attente par Gengo',
            'new'           => 'Nouvelle',
            'processing'    => 'En cours',
            'closed'        => 'Cloturée',
        );
    }

    public function getReviewableResponse() 
    {
        $order = $this->translationRequestOrders()->orderBy('created_at', 'desc')->first();
        $responses = array();

        foreach ($order->jobs as $job) {
            $responses[$job->slug] =  $job->callbackResponses()->orderBy('created_at', 'desc')->first();
        }

        return $responses;
    }

    public function getApprovedTranslatableValues($id)
    {
        $approvedTranslatables = array();

        $approvedOrder = $this->translationRequestOrders()
            ->where('id', $id)
            ->first();

        if($approvedOrder) {
            foreach ($approvedOrder->jobs as $job) {
                $keywords = preg_split("/_[0-9]+_+/", $job->slug);
                $slug = end($keywords);
                $approvedTranslatables[$slug] = $job->callbackResponses()->orderBy('created_at', 'desc')->first();
            }
        }

        return $approvedTranslatables;
    }

    public static function getApprovedTranslatableValueById($id)
    {
        $order = self::find($id);
        return $order->getApprovedTranslatableValues($id);
    }

    public function translationRequestOrders()
    {
        return $this->morphMany('Tokido\Dikateny\Model\TranslationRequestOrder', 'translatable');
    }

    public function applyTranslations($translations)
    {
        /*BuildKeyValue*/
        // \Log::info($translations);
        // \Log::info($this);
        $toSave = array();
        foreach ($translations as $key => $translation) {
            if (!empty($translation)) {
                if(array_key_exists($key, $this->autoTranslatables)) {
                    $explode = explode(':', $key);
                    if ($explode[0] === 'seo') { // SEO field
                        if ($this->seo) {
                            $this->seo->{$explode[1].':'.$translation->lc_tgt} = $translation->body_tgt;
                            $this->seo->save();
                        }
                    } else {
                        if (isset($this->type_event)) { // If evenement
                            if ($explode[0] == 'title' || $explode[0] == 'presentation'){
                                $this->single_evenement->{$key.':'.$translation->lc_tgt} = $translation->body_tgt;
                                $this->single_evenement->save();
                            } else {
                                $this->{$key.':'.$translation->lc_tgt} = $translation->body_tgt;
                            }
                        } else {
                            $this->{$key.':'.$translation->lc_tgt} = $translation->body_tgt;
                        }
                    }
                    
                }
            }
        }
        $this->update($toSave);
        $this->save();

        return $this;
    }

    public static function getLangueList()
    {
        $locales = \Config::get('app.locales');
        $result = [];

        if (is_array($locales)) {
            foreach ($locales as $ndx => $locale) {
                $result[$ndx] = $ndx;
            }
        }

        return $result;
    }

    public static function cancelAvailableTranslationRequestByOrderId($id)
    {
        if($id) {
            $order = TranslationRequestOrder::where('order_id', $id)->first();
            if($order) {
                $status =  static::$dikateny->cancelOrderByID($order->order_id);

                if(
                    is_array($status) 
                    && (
                        $status['opstat'] == 'ok'
                        // || (isset($status['err']) && $status['err']['code'] == 2755)
                    )
                ) {
                    
                    $order->status = 'canceled';
                    foreach ($order->jobs as $job) {
                        $job->status = 'canceled';
                        $job->save();
                    }
                    $order->save();

                    return true;
                } else {
                    return [
                        'error' => 'La traduction est déjà en cours.'
                    ];
                }
            } else {
                return [
                    'error' => 'La demande de traduction avec l\'id ' .$id. ' est introuvable.'
                ];
            }
        } else {
            return [
                'error' => 'Il y a un souci avec la demande de traduction que vouz voulez annuler.'
            ];
        }
    }

    public static function getLabel($field, $ucfirst = true)
    {
        $labels = [
            // Alert
            'title' => 'titre',
            'message' => 'message',
            'url_libelle' => 'texte du lien',
            'url_libelle2' => 'texte du lien N°2',

            // RelanceAdministrable
            'sub_title' => 'sous titre',
            'label' => 'texte du bouton',

            // MiseEnAvant (libre)
        ];

        if (isset($labels[$field])) {
            if ($ucfirst) {
                return ucfirst($labels[$field]);
            } else {
                return $labels[$field];
            }
        } else {
            return '';
        }

    }
}
