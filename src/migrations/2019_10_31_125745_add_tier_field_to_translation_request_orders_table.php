<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTierFieldToTranslationRequestOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('translation_request_orders', function (Blueprint $table) {
            $table->string('tier', 10)->nullable()->default('pro');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('translation_request_orders', function (Blueprint $table) {
            $table->dropColumn('tier');
        });
    }
}
