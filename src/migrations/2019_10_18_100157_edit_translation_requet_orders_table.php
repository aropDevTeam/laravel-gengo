<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditTranslationRequetOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('translation_request_orders', function (Blueprint $table) {
            $table->string('lc_src', 10)->nullable();
            $table->string('lc_tgt', 10)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('translation_request_orders', function (Blueprint $table) {
            $table->dropColumn('lc_src');
            $table->dropColumn('lc_tgt');
        });
    }
}
