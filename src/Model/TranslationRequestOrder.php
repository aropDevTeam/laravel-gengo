<?php 

namespace Tokido\Dikateny\Model;

Use Illuminate\Database\Eloquent\Model;

class TranslationRequestOrder extends Model
{
    protected $fillable = [
        'status',
        'order_id',
        'job_count',
        'credits_used',
        'currency',
        'wait_validation'
    ];

    public function translatable()
    {
        return $this->morphTo();
    }

    public function jobs()
    {
        return $this->hasMany('Tokido\Dikateny\Model\TranslationRequestJob', 'translation_request_order_id');
    }

    public function translationsComments()
    {
        return $this->morphMany('Tokido\Dikateny\Model\TranslationRequestOrder', 'commentable');
    }
}