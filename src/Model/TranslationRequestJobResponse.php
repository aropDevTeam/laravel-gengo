<?php 

namespace Tokido\Dikateny\Model;

Use Illuminate\Database\Eloquent\Model;

class TranslationRequestJobResponse extends Model
{

    protected $fillable = [
        'job_id', 
        'body_src', 
        'lc_src', 
        'lc_tgt', 
        'unit_count', 
        'tier', 
        'credits', 
        'status', 
        'eta', 
        'ctime', 
        'order_id', 
        'callback_url', 
        'auto_approve', 
        'preview_url', 
        'captcha_url', 
        'body_tgt'
    ];

    public function job()
    {
        return $this->belongsTo('Tokido\Dikateny\Model\TranslationRequestJob', 'translation_request_job_id');
    }
}